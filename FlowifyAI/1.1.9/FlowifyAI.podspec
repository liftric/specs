Pod::Spec.new do |s|
  s.name                    = 'FlowifyAI'
  s.version                 = '1.1.9'
  s.ios.deployment_target   = '12.0'
  s.vendored_frameworks     = 'FlowifyAI.xcframework'
  s.homepage                = 'https://gitlab.com/liftric/flowify-ai/flow-analysis-native'
  s.source                  = { :http => 'https://gitlab.com/api/v4/projects/26542646/packages/generic/flow-analysis-native/1.1.9/FlowifyAI.xcframework.zip', headers: ENV['CI_JOB_TOKEN'] ? ['JOB-TOKEN: ' + ENV['CI_JOB_TOKEN']] : ['PRIVATE-TOKEN: ' + ENV['PRIVATE_TOKEN']] }
  s.static_framework        = true
  s.frameworks              = 'UIKit', 'CoreGraphics', 'Accelerate'
  s.summary                 = 'FlowifyAI Obj-C wrapper'
  s.authors                 = { 'Ben John' => 'john@liftric.com', 'Jan Gaebel' => 'gaebel@liftric.com' }
end
