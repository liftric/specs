Pod::Spec.new do |s|
  s.name                    = 'FlowifyAI'
  s.version                 = '0.2.5'
  s.ios.deployment_target   = '12.0'
  s.vendored_frameworks     = "projects/darwin/FlowifyAI.xcframework"
  s.homepage                = 'https://gitlab.com/liftric/flowify-ai/flow-analysis-native'
  s.source                  = { :git => 'git@gitlab.com:liftric/flowify-ai/flow-analysis-native.git', :branch => 'refactor/multiplatform' }
  s.static_framework 		= true
  s.frameworks 				= 'UIKit', 'CoreGraphics'
  s.summary                 = 'FlowifyAI Obj-C wrapper'
  s.authors                 = { 'Ben John' => 'john@liftric.com', 'Jan Gaebel' => 'gaebel@liftric.com' }
end
